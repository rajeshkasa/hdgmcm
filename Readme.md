##### The code for HDGMCM algorithm submitted to Bioinformatics 2019 can be found in this repository. We also attach a sample implementation of it on the Glucoma dataset. 
The dataset and the required packages can be found in the `workspace to run the example.RData` file. The sample implementation can be found in `run and output of hdgmcm` file

==================

Please note that this code has not been optimized for performance.

For any further clarifications/feedback, please contact kasa@u.nus.edu
